# web-test-automation-verity

## Objetivo:
Automatizar um cenário de teste utilizando a notação Gherkin e a linguagem Java.

### Cenário original:
```
Dado que eu acessei a home page da Verity
Quando eu clicar na opção de menu “Contato”
E preencher os campos Nome Completo, Empresa, Email, Fone e Mensagem
E clicar no botão “Enviar”
Então o site irá registrar o envio apresentando uma mensagem de que a mensagem foi enviada com sucesso
```

### Cenário refatorado:
No exemplo acima, temos um cenário escrito com a notação Gherkin focado numa interação _step by step_ de uma página Web. Caso essa página se comunique com um serviço REST, por exemplo, não conseguiríamos reaproveitar esse cenário para realizar o teste do serviço.
O cenário abaixo foi ajustado para possuir um maior valor negocial da funcionalidade testada:
```
Funcionalidade: Contato

  Cenário: Enviando mensagem para a Verity com sucesso
    Dado que eu desejo entrar em contato com a Verity
    Quando eu acesso o menu de Contato na página da empresa
    E informo os campos Nome Completo, Empresa, Email, Telefone e Mensagem
    E confirmo o envio da mensagem
    Então eu devo receber uma confirmação de que a mensagem foi enviada com sucesso
```

### Tecnologias utilizadas:
  * Java
  * Maven
  * Selenium WebDriver
  * Junit
  * Cucumber
  * Java Faker

## Execução

### Pré-requisitos
* Clonar o repositório do projeto: `git clone https://gitlab.com/pablobedoya/web-test-automation-verity.git`
* Resolver as dependências do projeto com o comando Maven: `mvn clean install -DskipTests`

### Rodar os testes
* Rodar o comando Maven: `mvn test`