package verity.steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.E;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;
import verity.config.ConfigProperties;
import verity.data.ContactDataUtil;
import verity.driver.DriverFactory;
import verity.model.Contact;
import verity.page.ContactPage;

import static org.junit.Assert.assertTrue;

public class ContactSteps {

    private static final String SUCCES_MESSAGE = "Mensagem enviada com sucesso!";
    private Contact contact;
    private ContactPage contactPage;

    @Before
    public void setUp() {
        DriverFactory driverFactory = new DriverFactory();
        contactPage = new ContactPage(driverFactory.createInstance(ConfigProperties.getInstance().getBrowser()));
    }

    @Dado("que eu desejo entrar em contato com a Verity")
    public void que_eu_desejo_entrar_em_contato_com_a_verity() {
        contact = ContactDataUtil.generateContactData();
    }

    @Quando("eu acesso o menu de Contato na página da empresa")
    public void eu_acesso_o_menu_de_contato_na_página_da_empresa() {
        contactPage.navigateToContactPage();
    }

    @E("informo os campos Nome Completo, Empresa, Email, Telefone e Mensagem")
    public void informo_os_campos_nome_completo_empresa_email_telefone_e_mensagem() {
        contactPage.scrollToPageBottom();
        contactPage.fillAllFields(contact);
    }

    @E("confirmo o envio da mensagem")
    public void confirmo_o_envio_da_mensagem() {
        contactPage.clickSendButton();
    }

    @Então("eu devo receber uma confirmação de que a mensagem foi enviada com sucesso")
    public void eu_devo_receber_uma_confirmação_de_que_a_mensagem_foi_enviada_com_sucesso() {
        assertTrue(contactPage.successMessageIsPresent(SUCCES_MESSAGE));
    }

    @After
    public void tearDown() {
        contactPage.closeBrowser();
    }

}
