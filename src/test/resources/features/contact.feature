#language: pt

Funcionalidade: Contato

  Cenário: Enviando mensagem para a Verity com sucesso
    Dado que eu desejo entrar em contato com a Verity
    Quando eu acesso o menu de Contato na página da empresa
    E informo os campos Nome Completo, Empresa, Email, Telefone e Mensagem
    E confirmo o envio da mensagem
    Então eu devo receber uma confirmação de que a mensagem foi enviada com sucesso

