package verity.data;

import com.github.javafaker.Faker;
import verity.model.Contact;

public final class ContactDataUtil {

    private static Faker faker = new Faker();

    private ContactDataUtil() {
    }

    public static Contact generateContactData() {
        return Contact.builder()
                .fullName(generateFullName())
                .company(generateCompany())
                .email(generateEmail())
                .phone(generatePhone())
                .message(generateMessage())
                .build();
    }

    public static String generateFullName() {
        return faker.name().fullName();
    }

    public static String generateCompany() {
        return faker.company().name();
    }

    public static String generateEmail() {
        return faker.internet().emailAddress();
    }

    public static String generatePhone() {
        return faker.phoneNumber().phoneNumber();
    }

    public static String generateMessage() {
        return faker.lorem().paragraph();
    }

}
