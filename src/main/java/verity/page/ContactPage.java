package verity.page;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import verity.config.ConfigProperties;
import verity.model.Contact;

public class ContactPage extends BasePage {

    public ContactPage(WebDriver driver) {
        super(driver);
    }

    private static final String CONTACT_PATH = "/contato";

    @FindBy(xpath = "//input[@placeholder='Nome Completo']")
    private WebElement fullName;

    @FindBy(xpath = "//input[@placeholder='Empresa']")
    private WebElement company;

    @FindBy(xpath = "//input[@placeholder='E-mail']")
    private WebElement email;

    @FindBy(xpath = "//input[@placeholder='Fone']")
    private WebElement phone;

    @FindBy(xpath = "//textarea[@placeholder='Mensagem']")
    private WebElement message;

    @FindBy(xpath = "//button/*[text() = 'Enviar']")
    private WebElement sendButton;

    @FindBy(xpath = "//*/span")
    private WebElement successMessage;

    public void navigateToContactPage() {
        navigateTo(ConfigProperties.getInstance().getBaseUrl() + CONTACT_PATH);
    }

    private void waitUntilElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void scrollToPageBottom() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
    }

    public void fillFullName(String fullName) {
        waitUntilElementToBeClickable(this.fullName);
        this.fullName.clear();
        this.fullName.sendKeys(fullName);
    }

    public void fillCompany(String company) {
        waitUntilElementToBeClickable(this.company);
        this.company.clear();
        this.company.sendKeys(company);
    }

    public void fillEmail(String email) {
        waitUntilElementToBeClickable(this.email);
        this.email.clear();
        this.email.sendKeys(email);
    }

    public void fillPhone(String phone) {
        waitUntilElementToBeClickable(this.email);
        this.phone.clear();
        this.phone.sendKeys(phone);
    }

    public void fillMessage(String message) {
        waitUntilElementToBeClickable(this.message);
        this.message.clear();
        this.message.sendKeys(message);
    }

    public void clickSendButton() {
        sendButton.click();
    }

    public boolean successMessageIsPresent(String message) {
        return successMessage.getText().contentEquals(message);
    }

    public void fillAllFields(Contact contact) {
        fillFullName(contact.getFullName());
        fillCompany(contact.getCompany());
        fillEmail(contact.getEmail());
        fillPhone(contact.getPhone());
        fillMessage(contact.getMessage());
    }

}
