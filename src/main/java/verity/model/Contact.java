package verity.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Contact {

    private String fullName;

    private String company;

    private String email;

    private String phone;

    private String message;

}
